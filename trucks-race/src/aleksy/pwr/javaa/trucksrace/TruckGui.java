package aleksy.pwr.javaa.trucksrace;

import javax.swing.*;

public class TruckGui {
   private JPanel contentPane;

   public TruckGui() {
      JFrame frame = new JFrame("Trucks Race");
      frame.setContentPane(contentPane);
      frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
      frame.pack();
      frame.setVisible(true);
   }
}
