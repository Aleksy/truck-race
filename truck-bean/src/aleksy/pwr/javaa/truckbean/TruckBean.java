package aleksy.pwr.javaa.truckbean;

import javax.swing.*;
import java.awt.*;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Random;

public class TruckBean extends JPanel implements Externalizable {
   private final double DISTANCE_FROM = 10.0;
   private final double DISTANCE_TO = 300.0;
   private final double MAX_SPEED_FROM = 1.0;
   private final double MAX_SPEED_TO = 10.0;
   private final double ACCELERATION_FROM = 0.5;
   private final double ACCELERATION_TO = 3.0;
   private final double TANK_CAPACITY_FROM = 10.0;
   private final double TANK_CAPACITY_TO = 40.0;
   private final Font FONT = new Font("Press Start 2P", Font.PLAIN, 12);

   private String name;
   private Color color;
   private double distance;
   private double maxSpeed;
   private double acceleration;
   private double tankCapacity;
   private Double truckStartPointX;
   private Double truckStartPointY;
   private Double truckX;
   private boolean isInFinish;
   private double actualSpeed;
   private double actualFuel;


   public TruckBean() {
      this.name = "Truck Name";
      this.color = Color.YELLOW;
      this.distance = 300.0;
      this.maxSpeed = 3.;
      this.acceleration = 1.0;
      this.tankCapacity = 20.0;
      this.truckStartPointX = 20.0;
      this.truckStartPointY = 60.0;
      this.truckX = 0.0;
      this.actualSpeed = 0.0;
      this.actualFuel = this.tankCapacity;
      setForeground(Color.WHITE);
      setSizeDependingOnDistance();
      start();
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public Color getColor() {
      return color;
   }

   public void setColor(Color color) {
      this.color = color;
   }

   public double getDistance() {
      return distance;
   }

   public void setDistance(double distance) {
      this.distance = setBetween(distance, DISTANCE_FROM, DISTANCE_TO);
      setSizeDependingOnDistance();
   }

   public double getMaxSpeed() {
      return maxSpeed;
   }

   public void setMaxSpeed(double maxSpeed) {
      this.maxSpeed = setBetween(maxSpeed, MAX_SPEED_FROM, MAX_SPEED_TO);
   }

   public double getAcceleration() {
      return acceleration;
   }

   public void setAcceleration(double acceleration) {
      this.acceleration = setBetween(acceleration, ACCELERATION_FROM, ACCELERATION_TO);
   }

   public double getTankCapacity() {
      return tankCapacity;
   }

   public void setTankCapacity(double tankCapacity) {
      this.tankCapacity = setBetween(tankCapacity, TANK_CAPACITY_FROM, TANK_CAPACITY_TO);
      this.actualFuel = this.tankCapacity;
   }

   private double setBetween(double x, double from, double to) {
      if (x < from) {
         return from;
      }
      if (x > to) {
         return to;
      }
      return x;
   }

   @Override
   public void paintComponent(Graphics g) {
      super.paintComponent(g);
      drawTitle(g);
      drawFuelBar(g);
      drawTrailer(g);
      drawCabin(g);
      drawWheels(g);
      drawFinish(g);
   }

   private void drawFuelBar(Graphics g) {
      g.setColor(generateColor());
      g.fillRect(truckStartPointX.intValue(), truckStartPointY.intValue() - 20, (int) (100 * (actualFuel / tankCapacity)), 15);
      g.setColor(Color.BLACK);
      g.drawRect(truckStartPointX.intValue(), truckStartPointY.intValue() - 20, 100, 15);
   }

   private Color generateColor() {
      return new Color(Color.HSBtoRGB((float) ((0.4f * actualFuel / tankCapacity)),
         1.0f, 1.0f));
   }

   private void drawFinish(Graphics g) {
      int x = (int) (distance * 4.0) + 52;
      int y = truckStartPointY.intValue() - 7;
      for (int i = 0; i < 5; i++) {
         for (int j = 0; j < 4; j++) {
            if ((i + j) % 2 == 0)
               g.setColor(Color.BLACK);
            else
               g.setColor(Color.WHITE);
            g.fillRect(x, y, 5, 5);
            x += 5;
         }
         y += 5;
         x = (int) (distance * 4.0) + 52;
      }
   }

   private void drawWheels(Graphics g) {
      g.setColor(Color.BLACK);
      g.drawRect(truckX.intValue() + truckStartPointX.intValue() + 23, truckStartPointY.intValue() + 6, 4, 4);
      g.fillOval(truckX.intValue() + truckStartPointX.intValue() + 2, truckStartPointY.intValue() + 15, 4, 4);
      g.fillOval(truckX.intValue() + truckStartPointX.intValue() + 24, truckStartPointY.intValue() + 15, 4, 4);
   }

   private void drawCabin(Graphics g) {
      g.setColor(color.brighter());
      g.fillRect(truckX.intValue() + truckStartPointX.intValue() + 20, truckStartPointY.intValue() + 3, 10, 12);
      g.setColor(Color.BLACK);
      g.drawRect(truckX.intValue() + truckStartPointX.intValue() + 20, truckStartPointY.intValue() + 3, 10, 12);
      drawWindow(g);
   }

   private void drawWindow(Graphics g) {
      g.setColor(Color.CYAN);
      g.fillRect(truckX.intValue() + truckStartPointX.intValue() + 23, truckStartPointY.intValue() + 6, 4, 4);
   }

   private void drawTrailer(Graphics g) {
      g.setColor(color);
      g.fillRect(truckX.intValue() + truckStartPointX.intValue(), truckStartPointY.intValue(), 20, 15);
      g.setColor(Color.BLACK);
      g.drawRect(truckX.intValue() + truckStartPointX.intValue(), truckStartPointY.intValue(), 20, 15);
   }

   private void drawTitle(Graphics g) {
      g.setColor(Color.BLACK);
      g.setFont(FONT);
      g.drawString(name + " sp: " + round(actualSpeed), 30, 30);
   }

   private void setSizeDependingOnDistance() {
      setSize((int) (distance * 4) + 100, 120);
      setPreferredSize(new Dimension((int) (distance * 4) + 100, 120));
   }


   @Override
   public void writeExternal(ObjectOutput out) throws IOException {
      out.writeObject(name);
      out.writeObject(color);
      out.writeObject(distance);
      out.writeObject(maxSpeed);
      out.writeObject(acceleration);
      out.writeObject(tankCapacity);
   }

   @Override
   public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
      this.setName((String) in.readObject());
      this.setColor((Color) in.readObject());
      this.setDistance(in.readDouble());
      this.setMaxSpeed(in.readDouble());
      this.setAcceleration(in.readDouble());
      this.setTankCapacity(in.readDouble());
   }

   public void start() {
      new Thread() {
         private TruckBean parent;

         public Thread setParent(TruckBean parent) {
            this.parent = parent;
            return this;
         }

         @Override
         public void run() {
            while (!parent.isInFinish) {
               try {
                  Thread.sleep(10);
               } catch (InterruptedException e) {
                  e.printStackTrace();
               }
               parent.actualFuel -= 0.0237491;
               if (parent.truckX >= parent.distance * 4.0) {
                  parent.isInFinish = true;
               }
               parent.actualSpeed += 0.01 * parent.acceleration;
               if (parent.actualSpeed >= parent.maxSpeed) {
                  parent.actualSpeed = parent.maxSpeed;
               }
               if(parent.actualFuel <= 0.0) {
                  parent.actualSpeed = 0.0;
                  try {
                     Thread.sleep(1000);
                  } catch (InterruptedException e) {
                     e.printStackTrace();
                  }
                  parent.actualFuel = parent.tankCapacity;
               }
               parent.truckX += 0.1 * parent.actualSpeed;
               parent.repaint();
            }
         }
      }.setParent(this).start();
   }

   private double round(double x) {
      x *= 100.0;
      x = Math.round(x);
      return x / 100.0;
   }
}
