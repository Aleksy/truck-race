package aleksy.pwr.javaa.truckbean;

import java.beans.BeanDescriptor;
import java.beans.SimpleBeanInfo;

public class TruckBeanBeanInfo extends SimpleBeanInfo {
   @Override
   public BeanDescriptor getBeanDescriptor() {
      BeanDescriptor beanDescriptor = new BeanDescriptor(TruckBean.class);
      beanDescriptor.setDisplayName("Truck");
      return beanDescriptor;
   }
}
